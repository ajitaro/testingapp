/* eslint-disable prettier/prettier */
import React from 'react'
import { View, Text, StyleSheet, Image, Dimensions } from 'react-native'
import data from './ShopTheLook.json'

const width = Dimensions.get('window').width; //full width

const button = data.data[0].button
const products = data.data[0].products

const App = () => {
  console.log(data.data[0])
  return (
    <View style={styles.kotak}>
      <Image source={require('./image.jpg')} style={{width: width, height: '100%', resizeMode: 'cover'}} />
      {products.map((product, index) => (
        <View key={index} style={[styles.button, {top: `${product.style.top}%`, left: `${product.style.left}%`}]}/>
      ))}
    </View>
  )
}

const styles = StyleSheet.create({
  kotak: {
    backgroundColor: '#888888',
    height: '50%'
  },
  button: {
    backgroundColor: button.button_color,
    height: 30,
    width: 30,
    borderRadius: 25,
    position: 'absolute',
  }
})

export default App
